# Geração de Assinatura Médica

Este é um exemplo integração dos serviços da API de assinatura com clientes baseados em tecnologia PHP.

Este exemplo apresenta os passos necessários para a geração de metadados referentes ao médico na prescrição médica.

  - Passo 1: Criação do formulário com o documento que será assinado.
  - Passo 2: Recebimento do documento com os metadados.
  - Passo 3: O documento recebido deverá ser enviado para uma assinatura do tipo PDF/PADES.

### Assinatura PDF

Após ter os metadados inseridos, o documento deverá ser enviado novamente ao HUB, para que seja realizada de fato a assinatura. Aqui estão exemplos de como realizar a assinatura PDF:

* [Com certificado na nuvem](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-de-assinatura-pdf-com-certificado-na-nuvem-kms/back-end)
* [Com certificado no navegador](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-de-assinatura-pdf-com-certificado-no-navegador/back-end)
* [Com certificado em disco](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/assinatura-pdf-chave-disco-pkcs12)

### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [CURL] - Client URL Library.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| token | Access Token para o consumo do serviço (JWT). |     AssinaturaMedica.php    | 

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

### Uso

Para execução da aplicação de exemplo é necessário ter o PHP 7.x instalado na sua máquina, além de instalar a biblioteca php-curl.

Comandos:

Instalar php-curl (linux):

    -apt-get install php-curl

Caso o seu sistema operacional seja outro, verifique sobre a instalação [aqui](https://www.php.net/manual/pt_BR/curl.requirements.php).

Executar programa:

    -php AssinaturaMedica.php


   [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
   [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>